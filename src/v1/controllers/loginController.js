const loginService = require("../services/loginService")
const jwtSerivce = require("../services/jwtService")

const registerUser = async (req, res) => {
    const user = await loginService.registerUser(req)
    res.send(user)
}

const getUser = async (req, res) => {
    var user
    if (req.query.email) {
        user = await loginService.getUserByEmail(req.query.email)
    } else if (req.query.username) {
        user = await loginService.getUserByUsername(req.query.username)
    }

    if (user) {
        res.send(user)
    } else {
        res.status(404).send({ status: "FAILURE", message: "User not found"})
    }
}

const getUsers = async (req, res) => {
    const users = await loginService.getUsers()
    res.send(users)
}

const login = async (req, res) => {
    const user = await loginService.login(req)
    if (user) {
        var tokenData = await jwtSerivce.generateToken(user)
        if (tokenData) {
            user.token = tokenData.token
            user.refreshToken = tokenData.refreshToken
        }
        res.send(user)
    } else {
        res.status(400).send({ status: "INVALID", message: "Invalid user or password"})
    }
}

const getToken = async (req, res) => {
    const authHeader = req.headers.authorization
    const email = req.body.email
    if (!authHeader || authHeader.indexOf('Basic ') === -1) {
        return res.status(401).json({ status: 'FAILURE', message: 'Missing Authorization Header' })
    } else {
        return jwtSerivce.getToken(email, authHeader)
    }
}

module.exports = {
    registerUser,
    getUser,
    getUsers,
    getToken,
    login
}