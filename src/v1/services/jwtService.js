const jwt = require('jsonwebtoken')
var uuid = require('uuid')
const LoginService = require("./loginService")
const RefreshTokenModel = require("../models/refreshToken")

const getToken = async(email, authHeader) => {
    const base64Credentials = authHeader.split(' ')[1]
    const credentials = Buffer.from(base64Credentials, 'base64').toString('ascii')
    const [key, secret] = credentials.split(':')
    try {
        var user = null
        if (key != undefined && secret != undefined) {
            var appSecret = await LoginService.getUserByEmail(email)
            if (appSecret != null && appSecret.secret == secret) {
                user = await LoginService.getUserByEmail(email)
                if (user) {
                    return await generateToken(user);
                } else {
                    return 'Invalid email or password'
                }
            } else {
                return 'Invalid Secret'
            }
        } else {
            return 'parameters are missing or invalid'
        }
    } catch (error) {
        console.error("jwtToken -> getToken -> error occurred = " + error.message)
        return error.message
    }
}

const generateToken = async (user) => {
    var jwtRefreshToken = uuid.v4()
    var query = { email: user.email }
    var result = await RefreshTokenModel.findOne(query);
    if (result == null) {
        const expiresIn = 2592000
        var expiresAt = new Date()
        expiresAt.setTime(expiresAt.getTime() + expiresIn)
        var refreshData = {
            email: user.email,
            token: jwtRefreshToken,
            expiresAt: expiresAt
        }
        const refreshToken = new RefreshTokenModel(refreshData)
        await refreshToken.save()
    } else {
        jwtRefreshToken = result.token
    }

    var param = { "name": user.name, "email": user.email, "role": user.role }
    var jwtToken = jwt.sign(param, process.env.TOKEN_SECRET, { expiresIn: '1d' })

    var token = { "token": jwtToken, "refreshToken": jwtRefreshToken }
    return token;
}

const authenticateJWT = async(req, res, next) => {
    const authHeader = req.headers.authorization
    if (authHeader) {
        const token = authHeader.split(' ')[1]
        jwt.verify(token, process.env.TOKEN_SECRET, (err, user) => {
            if (err) {
                if (err.name == "JsonWebTokenError") {
                    return res.status(401).send({ status: 'UNAUTHORIZED', message: err.message })
                } else if (err.name == "TokenExpiredError") {
                    return res.status(401).send({ status: 'UNAUTHORIZED', message: err.message })
                } else {
                    return res.status(401).send({ status: 'UNAUTHORIZED', message: 'Invalid Access' })
                }
            }
            req.user = user
            next()
        })
    } else {
        res.status(401).send({ status: 'UNAUTHORIZED', message: 'Invalid Token' })
    }
}

module.exports = { 
    getToken, 
    authenticateJWT,
    generateToken,
}
