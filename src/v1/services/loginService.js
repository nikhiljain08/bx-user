const UserModel = require("../models/user")
const bcrypt = require("bcrypt")

const registerUser = async (req) => {
	const user = new UserModel(req.body)
	try {
		await user.save()
		return userMapper(user)
	} catch (error) {
		console.log(error)
		return error
	}
}

const getUsers = async () => {
	const users = await UserModel.find({})
	try {
		return users.map((user) => userMapper(user))
	} catch (error) {
		return error
	}
}

const getUserByEmail = async (email) => {
	const user = await UserModel.findOne({ email: email })
	return user != null ? userMapper(user) : null
}

const getUserByUsername = async (username) => {
	const user = await UserModel.findOne({ username: username })
	return user != null ? userMapper(user) : null
}

const login = async (req) => {
	const user = await UserModel.findOne({ email: req.body.email })
	var result = await bcrypt.compare(req.body.password, user.password)
	return result ? userMapper(user) : null
}

const userMapper = (user) => {
	var response = {
		name: user.name,
		email: user.email,
		username: user.username,
		role: user.role,
		createdAt: user.createdAt,
		updatedAt: user.updatedAt
	}
	return response
}

module.exports = {
	registerUser,
	getUsers,
	getUserByEmail,
	getUserByUsername,
	login,
	userMapper,
}
