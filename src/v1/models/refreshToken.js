const mongoose = require("mongoose")

const refreshTokenSchema = mongoose.Schema(
	{
		email: {
			type: String,
			required: true,
			trim: true,
			index: { unique: true },
		},
		token: {
			type: String,
			required: true,
			trim: true,
		},
		expiresAt: {
			type: Date,
			default: Date.now
		}
	},
	{
		versionKey: false,
	}
)

const RefreshToken = mongoose.model("refreshToken", refreshTokenSchema)

module.exports = RefreshToken
