const mongoose = require("mongoose")
const bcrypt = require("bcrypt")
const SALT_WORK_FACTOR = 10

const UserSchema = mongoose.Schema(
	{
		username: {
			type: String,
			required: true,
			trim: true,
			index: { unique: true },
		},
		email: {
			type: String,
			required: true,
			trim: true,
			index: { unique: true },
		},
		password: {
			type: String,
			required: true,
			validate: {
				validator: checkCredentials,
				message: props => `${props.value} is not a valid phone number!`
			},
		},
		name: {
			firstName: {
				type: String,
				trim: true,
				required: true,
			},
			lastName: {
				type: String,
				trim: true,
				required: true,
			},
		},
		role: {
			type: String,
			required: true,
			trim: true
		},
		createdAt: {
			type: Date,
			default: Date.now
		},
		updatedAt: {
			type: Date,
			default: Date.now
		}
	},
	{
		versionKey: false,
	}
)

UserSchema.pre("save", function (next) {
	var user = this

	if (!user.isModified("password")) return next()

	bcrypt.genSalt(SALT_WORK_FACTOR, (err, salt) => {
		if (err) return next(err)

		bcrypt.hash(user.password, salt, (err, hash) => {
			if (err) return next(err)
			user.password = hash
			next()
		})
	})
})


UserSchema.pre('validate', function(next) {
	next()
    // if (!this.password) {
    //     next(new Error('You should provide a password'))
    // } else {
    //     next()
    // }
})

function checkCredentials(value) {
	if (!this.password) {
		return false
	}
	return true
 }

const User = mongoose.model("user", UserSchema)

module.exports = User
