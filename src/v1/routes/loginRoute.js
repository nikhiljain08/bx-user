const express = require("express")
const router = express.Router()
const loginController = require("../controllers/loginController")
const jwtToken = require('../services/jwtService');

router.post("/register", loginController.registerUser)
router.get("/getUser", loginController.getUser)
router.get("/getUsers", jwtToken.authenticateJWT, loginController.getUsers)
router.get("/token", loginController.getUsers)
router.post("/", loginController.login)

module.exports = router