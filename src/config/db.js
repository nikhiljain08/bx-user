const mongoose = require('mongoose')
const connectionString = process.env.ATLAS_URI

const connectDB = async () => {
    try{
        const conn = await mongoose.connect(process.env.ATLAS_URI, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useCreateIndex: true,
            useFindAndModify: true
        })
        console.log(`MongoDB connected successfully : ${conn.connection.host}`)
    } catch (err) {
        console.error(err)
        process.exit(1)
    }
}

module.exports = connectDB