const dotenv = require('dotenv')
const express = require('express')
const connectDB = require('./config/db')
const cors = require('cors')
const loginRouter = require("./v1/routes/loginRoute")

dotenv.config()

connectDB()
const app = express()
const port = process.env.PORT || 3000
app.use(cors())
app.use(express.json())

app.use("/api/v1/login", loginRouter)

app.listen(port, () => {
  console.log(`Buxwave User Management listening on port ${port}`)
})

app.use(function (err, _req, res) {
  console.error(err.stack)
  res.status(500).send('Something broke!')
});
